layui.use(['form','layer'],function(){
    var form = layui.form
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery;

    form.on("submit(addUser)",function(data){
        var url=(data.field.uid == "" || data.field.uid ==null) ? "/register.action" : "/updateuser.action";
        var addusers=JSON.stringify(data.field);
        //alert(addusers);
        //alert(url);
        console.log(data.field);
        console.log(addusers);
        $.post(url,data.field,function (data) {
            //alert(data);
            if(data.data=="success"){
                layer.msg("操作成功");
            }else if(data.data=="shibai"){
                layer.msg("用户名或账号已存在，请重新输入");
            }
        });
        setTimeout(function(){  //使用  setTimeout（）方法设定定时2000毫秒
            //关闭页面
            layer.closeAll("iframe");
            //刷新父页面
            parent.location.reload();
        },2000);
        return false;
    });

    //格式化时间
    function filterTime(val){
        if(val < 10){
            return "0" + val;
        }else{
            return val;
        }
    }
    //定时发布
    var time = new Date();
    var submitTime = time.getFullYear()+'-'+filterTime(time.getMonth()+1)+'-'+filterTime(time.getDate())+' '+filterTime(time.getHours())+':'+filterTime(time.getMinutes())+':'+filterTime(time.getSeconds());

});