layui.use(['form','layer','table','laytpl','laypage'],function(){
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        laytpl = layui.laytpl,
        table = layui.table,
        laypage=layui.laypage;

    //用户列表
    var tableIns = table.render({
        elem: '#userList',
        url : '/pagefind.action',
        cellMinWidth : 95,
        page : true,
        height : "full-125",
        limits : [10,15,20],
        limit : 10,
        id : "userListTable",
        cols : [[
            {type: "checkbox", fixed:"left", width:50},
            {field: 'account', title: '账号', minWidth:100, align:"center"},
            {field: 'password', title: '密码', minWidth:100, align:'center'},
            {field: 'username', title: '用户名', minWidth:100, align:'center'},
            {field: 'sex', title: '性别', minWidth:100,align:'center'},
            {field: 'startus', title: '状态',  minWidth:100,align:'center',templet:function(d){
                return d.startus == "0" ? "正常使用" : "限制使用";
            }},
            {field: 'createtime', title: '注册时间', minWidth:100,align:'center',templet:function (d) {
                    return createTime(d.createtime);
                }},
            {title: '操作', minWidth:175, templet:'#userListBar',fixed:"right",align:"center"}
        ]],
    });
    //时间转换函数
    function createTime(v){
        var date = new Date(v);
        var y = date.getFullYear();
        var m = date.getMonth()+1;
        m = m<10?'0'+m:m;
        var d = date.getDate();
        d = d<10?("0"+d):d;
        var h = date.getHours();
        h = h<10?("0"+h):h;
        var M = date.getMinutes();
        M = M<10?("0"+M):M;
        var str = y+"-"+m+"-"+d+" "+h+":"+M;
        return str;
    }
    $(".search_btn").on("click",function(){
        if($(".searchVal").val() != ''){
            table.reload("userListTable",{
                page: {
                    curr: 1 //重新从第 1 页开始
                },
                where: {
                    username: $(".searchVal").val()  //搜索的关键字
                }
            })
        }else{
            layer.msg("请输入搜索的内容");
        }
    });

    //添加用户
    function addUser(edit){
        var index = layui.layer.open({
            title : "添加用户",
            type : 2,
            content : "userAdd.html",
            success : function(layero, index){
                var body = layui.layer.getChildFrame('body', index);
                if(edit){
                    body.find(".uid").val(edit.uid);  //id
                    body.find(".account").val(edit.account);  //账号
                    body.find(".password").val(edit.password);  //密码
                    body.find(".username").val(edit.username);  //用户名
                    /*body.find(".userSex input[value="+edit.sex+"]").prop("checked","checked");  //性别*/
                    body.find(".startus").val(edit.startus);  //用户状态
                    form.render();
                }
                setTimeout(function(){
                    layui.layer.tips('点击此处返回用户列表', '.layui-layer-setwin .layui-layer-close', {
                        tips: 3
                    });
                },500)
            }
        })
        layui.layer.full(index);
        //改变窗口大小时，重置弹窗的宽高，防止超出可视区域（如F12调出debug的操作）
        $(window).on("resize",function(){
            layui.layer.full(index);
        })
    }
    $(".addNews_btn").click(function(){
        addUser();
    });

    //批量删除
    $(".delAll_btn").click(function(){
        var checkStatus = table.checkStatus('userListTable'),
            data = checkStatus.data,
            newsId = [];
        if(data.length > 0) {
            for (var i in data) {
                newsId.push(data[i].uid);
                //alert(data[i].uid);
            }
            layer.confirm('确定删除选中的用户？', {icon: 3, title: '提示信息'}, function (index) {
                $.ajax({
                    url:"/deleteuser.action",
                    type:"post",
                    data:JSON.stringify(newsId),
                    // 定义发送请求的数据格式为JSON字符串
                    contentType : "application/json;charset=UTF-8",
                    //定义回调响应的数据格式为JSON字符串,该属性可以省略
                    dataType : "json",
                    success:function (data) {
                        if(data.code===0){
                            alert("删除成功");
                        }else{
                            alert("删除失败");
                        }
                        //刷新页面
                        tableIns.reload();
                        layer.close(index);
                    }
                });
            })
        }else{
            layer.msg("请选择需要删除的用户");
        }
    })

    //列表操作
    table.on('tool(userList)', function(obj){
        var layEvent = obj.event,
            data = obj.data;
            newid=[];
            newid.push(obj.data.uid);
        if(layEvent === 'edit'){ //编辑
            addUser(data);
        }else if(layEvent === 'usable'){ //启用禁用
            var _this = $(this),
                usableText = "是否确定禁用此用户？",
                btnText = "已禁用";
            if(_this.text()=="已禁用"){
                usableText = "是否确定启用此用户？",
                btnText = "已启用";
            }
            layer.confirm(usableText,{
                icon: 3,
                title:'系统提示',
                cancel : function(index){
                    layer.close(index);
                }
            },function(index){
                _this.text(btnText);
                layer.close(index);
            },function(index){
                layer.close(index);
            });
        }else if(layEvent === 'del'){ //删除
            layer.confirm('确定删除此用户？',{icon:3, title:'提示信息'},function(index){
                $.ajax({
                    url:"/deleteuser.action",
                    type:"post",
                    data:JSON.stringify(newid),
                    contentType : "application/json;charset=UTF-8",
                    dataType : "json",
                    success:function (data) {
                        if(data.code===0){
                            alert("删除成功");
                        }else{
                            alert("删除失败");
                        }
                        //刷新页面
                        tableIns.reload();
                        layer.close(index);
                    }
                });
            });
        }
    });

})