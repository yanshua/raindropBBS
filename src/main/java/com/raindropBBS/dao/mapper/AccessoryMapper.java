package com.raindropBBS.dao.mapper;

import com.raindropBBS.dao.po.Accessory;
import com.raindropBBS.dao.po.AccessoryExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AccessoryMapper {
    int countByExample(AccessoryExample example);

    int deleteByExample(AccessoryExample example);

    int deleteByPrimaryKey(Long acid);

    int insert(Accessory record);

    int insertSelective(Accessory record);

    List<Accessory> selectByExampleWithBLOBs(AccessoryExample example);

    List<Accessory> selectByExample(AccessoryExample example);

    Accessory selectByPrimaryKey(Long acid);

    int updateByExampleSelective(@Param("record") Accessory record, @Param("example") AccessoryExample example);

    int updateByExampleWithBLOBs(@Param("record") Accessory record, @Param("example") AccessoryExample example);

    int updateByExample(@Param("record") Accessory record, @Param("example") AccessoryExample example);

    int updateByPrimaryKeySelective(Accessory record);

    int updateByPrimaryKeyWithBLOBs(Accessory record);

    int updateByPrimaryKey(Accessory record);
}