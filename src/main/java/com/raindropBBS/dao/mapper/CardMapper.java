package com.raindropBBS.dao.mapper;

import com.raindropBBS.dao.po.Card;
import com.raindropBBS.dao.po.CardExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CardMapper {
    int countByExample(CardExample example);

    int deleteByExample(CardExample example);

    int deleteByPrimaryKey(Long cid);

    int insert(Card record);

    int insertSelective(Card record);

    List<Card> selectByExampleWithBLOBs(CardExample example);

    List<Card> selectByExample(CardExample example);

    Card selectByPrimaryKey(Long cid);

    int updateByExampleSelective(@Param("record") Card record, @Param("example") CardExample example);

    int updateByExampleWithBLOBs(@Param("record") Card record, @Param("example") CardExample example);

    int updateByExample(@Param("record") Card record, @Param("example") CardExample example);

    int updateByPrimaryKeySelective(Card record);

    int updateByPrimaryKeyWithBLOBs(Card record);

    int updateByPrimaryKey(Card record);
}