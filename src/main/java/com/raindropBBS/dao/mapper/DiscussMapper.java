package com.raindropBBS.dao.mapper;

import com.raindropBBS.dao.po.Discuss;
import com.raindropBBS.dao.po.DiscussExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DiscussMapper {
    int countByExample(DiscussExample example);

    int deleteByExample(DiscussExample example);

    int deleteByPrimaryKey(Long did);

    int insert(Discuss record);

    int insertSelective(Discuss record);

    List<Discuss> selectByExampleWithBLOBs(DiscussExample example);

    List<Discuss> selectByExample(DiscussExample example);

    Discuss selectByPrimaryKey(Long did);

    int updateByExampleSelective(@Param("record") Discuss record, @Param("example") DiscussExample example);

    int updateByExampleWithBLOBs(@Param("record") Discuss record, @Param("example") DiscussExample example);

    int updateByExample(@Param("record") Discuss record, @Param("example") DiscussExample example);

    int updateByPrimaryKeySelective(Discuss record);

    int updateByPrimaryKeyWithBLOBs(Discuss record);

    int updateByPrimaryKey(Discuss record);
}