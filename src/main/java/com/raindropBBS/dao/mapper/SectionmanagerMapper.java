package com.raindropBBS.dao.mapper;

import com.raindropBBS.dao.po.Sectionmanager;
import com.raindropBBS.dao.po.SectionmanagerExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SectionmanagerMapper {
    int countByExample(SectionmanagerExample example);

    int deleteByExample(SectionmanagerExample example);

    int deleteByPrimaryKey(Long smid);

    int insert(Sectionmanager record);

    int insertSelective(Sectionmanager record);

    List<Sectionmanager> selectByExample(SectionmanagerExample example);

    Sectionmanager selectByPrimaryKey(Long smid);

    int updateByExampleSelective(@Param("record") Sectionmanager record, @Param("example") SectionmanagerExample example);

    int updateByExample(@Param("record") Sectionmanager record, @Param("example") SectionmanagerExample example);

    int updateByPrimaryKeySelective(Sectionmanager record);

    int updateByPrimaryKey(Sectionmanager record);
}