package com.raindropBBS.dao.mapper;

import com.raindropBBS.dao.po.Systeminfo;
import com.raindropBBS.dao.po.SysteminfoExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysteminfoMapper {
    int countByExample(SysteminfoExample example);

    int deleteByExample(SysteminfoExample example);

    int insert(Systeminfo record);

    int insertSelective(Systeminfo record);

    List<Systeminfo> selectByExampleWithBLOBs(SysteminfoExample example);

    List<Systeminfo> selectByExample(SysteminfoExample example);

    int updateByExampleSelective(@Param("record") Systeminfo record, @Param("example") SysteminfoExample example);

    int updateByExampleWithBLOBs(@Param("record") Systeminfo record, @Param("example") SysteminfoExample example);

    int updateByExample(@Param("record") Systeminfo record, @Param("example") SysteminfoExample example);
}