package com.raindropBBS.dao.mapper;

import com.raindropBBS.dao.po.User;
import com.raindropBBS.dao.po.UserExample;
import com.raindropBBS.util.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserMapper {
    int countByExample(UserExample example);

    int deleteByExample(UserExample example);

    int deleteByPrimaryKey(Long uid);

    int insert(User record);

    int insertSelective(User record);

    List<User> selectByExample(UserExample example);

    User selectByPrimaryKey(Long uid);

    int updateByExampleSelective(@Param("record") User record, @Param("example") UserExample example);

    int updateByExample(@Param("record") User record, @Param("example") UserExample example);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    public List<User> userlist()throws Exception;

    public List<User> login()throws Exception;

    public void register(User user)throws Exception;

    //删除用户
    public void deleteuser(Long[] id)throws Exception;

    public void updateuser(User user)throws Exception;

    public List<User> pages(Page page)throws Exception;
}