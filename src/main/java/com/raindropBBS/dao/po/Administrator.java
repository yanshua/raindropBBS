package com.raindropBBS.dao.po;

import java.util.Date;

public class Administrator {
    private Long aid;

    private String account;

    private String password;

    private Date updatetime;

    private Date createtiem;

    public Long getAid() {
        return aid;
    }

    public void setAid(Long aid) {
        this.aid = aid;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account == null ? null : account.trim();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    public Date getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Date updatetime) {
        this.updatetime = updatetime;
    }

    public Date getCreatetiem() {
        return createtiem;
    }

    public void setCreatetiem(Date createtiem) {
        this.createtiem = createtiem;
    }
}