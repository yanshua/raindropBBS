package com.raindropBBS.dao.po;

import java.util.Date;

public class Sectionmanager {
    private Long smid;

    private Long sid;

    private Long uid;

    private Date updatetime;

    private Date creatime;

    public Long getSmid() {
        return smid;
    }

    public void setSmid(Long smid) {
        this.smid = smid;
    }

    public Long getSid() {
        return sid;
    }

    public void setSid(Long sid) {
        this.sid = sid;
    }

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public Date getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Date updatetime) {
        this.updatetime = updatetime;
    }

    public Date getCreatime() {
        return creatime;
    }

    public void setCreatime(Date creatime) {
        this.creatime = creatime;
    }
}