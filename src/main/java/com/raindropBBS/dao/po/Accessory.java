package com.raindropBBS.dao.po;

import java.util.Date;

public class Accessory {
    private Long acid;

    private Long uid;

    private Long cid;

    private Integer accessoryscore;

    private Date updatetime;

    private Date creatime;

    private String accessorycontent;

    public Long getAcid() {
        return acid;
    }

    public void setAcid(Long acid) {
        this.acid = acid;
    }

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public Long getCid() {
        return cid;
    }

    public void setCid(Long cid) {
        this.cid = cid;
    }

    public Integer getAccessoryscore() {
        return accessoryscore;
    }

    public void setAccessoryscore(Integer accessoryscore) {
        this.accessoryscore = accessoryscore;
    }

    public Date getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Date updatetime) {
        this.updatetime = updatetime;
    }

    public Date getCreatime() {
        return creatime;
    }

    public void setCreatime(Date creatime) {
        this.creatime = creatime;
    }

    public String getAccessorycontent() {
        return accessorycontent;
    }

    public void setAccessorycontent(String accessorycontent) {
        this.accessorycontent = accessorycontent == null ? null : accessorycontent.trim();
    }
}