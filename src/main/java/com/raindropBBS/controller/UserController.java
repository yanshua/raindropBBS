package com.raindropBBS.controller;

import com.raindropBBS.dao.po.User;
import com.raindropBBS.service.UserService;
import com.raindropBBS.util.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Title: raindropBBS
 * @Description: java类作用描述
 * @Author: 杨帅
 * @CreateDate: 2018/10/15 15:02
 * @Version: 1.0
 */
@Controller
public class UserController {
    @Resource
    UserService userService;

    @RequestMapping(value = "userlist",produces = "application/json;charset=utf-8")
    @ResponseBody
    public Object userlist()throws Exception{
        List<User> userlist=userService.userlist();
        System.out.println("数据"+userlist);
        Map<String,Object> map=new HashMap<String, Object>();
        map.put("code", 0);
        map.put("msg", "");
        map.put("count",3);
        map.put("data",userlist);
        return map;
    }
    //登录
    @RequestMapping(value = "login",produces = "application/json;charset=utf-8")
    @ResponseBody
    public Object login(String account,String password)throws Exception{
        String result=userService.login(account,password);
        Map<String,Object> map=new HashMap<String, Object>();
        map.put("data",result);
        return map;
    }
    //注册
    @RequestMapping(value = "register",produces = "application/json;charset=utf-8")
    @ResponseBody
    public Object register(User user)throws Exception{
        System.out.println(user);
        String result=null;
        result=userService.register(user);
        System.out.println("注册成功");
        Map<String,Object> map=new HashMap<String, Object>();
        map.put("data",result);
        return map;
    }

    //删除用户
    @RequestMapping(value = "deleteuser",produces = "application/json;charset=utf-8")
    @ResponseBody
    public Object deleteuser(@RequestBody Long[] id) throws Exception {
        userService.deleteuser(id);
        System.out.println("删除成功");
        //返回提示
        Map<String, Object> userMap = new HashMap<String, Object>();
        userMap.put("code", 0);
        return userMap;
    }

    //更新用户
    @RequestMapping(value = "updateuser",produces = "application/json;charset=utf-8")
    @ResponseBody
    public Object updateuser(User user) throws Exception {
        userService.updateuser(user);
        //返回提示
        System.out.println("更新成功");
        Map<String, Object> userMap = new HashMap<String, Object>();
        userMap.put("data","success");
        return userMap;
    }

    //用户分页
    @RequestMapping(value = "pagefind",produces = "application/json;charset=utf-8")
    @ResponseBody
    public Object pagefind(String username,int page,int limit) throws Exception {
        List<User> users=userService.pages(username,page,limit);
        List<User> users1=userService.userlist();
        String um=username;   //判断搜索条件是否为空
        int count=0;
        if(um==null||um==""){
            count=users1.size();
        }else{
            count=users.size();
        }
        System.out.println("分页成功");
        //返回结果
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("code", 0);
        map.put("msg", "");
        map.put("count",count);
        map.put("data",users);
        return map;
    }
}
