package com.raindropBBS.service.impl;

import com.raindropBBS.dao.mapper.UserMapper;
import com.raindropBBS.dao.po.User;
import com.raindropBBS.service.UserService;
import com.raindropBBS.util.DataConverter;
import com.raindropBBS.util.MD5Utils;
import com.raindropBBS.util.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.crypto.Data;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @Title: raindropBBS
 * @Description: java类作用描述
 * @Author: 杨帅
 * @CreateDate: 2018/10/15 15:01
 * @Version: 1.0
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserMapper userMapper;

    @Override
    public List<User> userlist() throws Exception {
        return userMapper.userlist();
    }
    //登录
    @Override
    public String login(String account,String password) throws Exception {
        List<User> user=userMapper.login();
        System.out.println(user);
        String result=null;
        for (User users:user) {
            MD5Utils md5Utils=new MD5Utils();
            String passwords=md5Utils.MD5Encode(password,"UTF-8");
            if(users.getPassword().equals(passwords) && users.getAccount().equals(account)){
                result="success";
            }
        }
        return result;
    }
    //注册
    @Override
    public String register(User user) throws Exception {
        String account=user.getAccount();
        String username=user.getUsername();
        String result=null;
        //判断用户名或账号已经被注册
        List<User> users=userMapper.userlist();
        for(int i=0;i<users.size();i++){
            if(users.get(i).getAccount().equals(account)|| users.get(i).getUsername().equals(username)){
                result="shibai";
                break;
            }else if(i==(users.size()-1)){
                String password=user.getPassword();
                MD5Utils md5Utils=new MD5Utils();
                String passwords=md5Utils.MD5Encode(password,"UTF-8");
                user.setPassword(passwords);
                Date time=new Date();
                user.setCreatetime(time);
                user.setUpdatetime(time);
                userMapper.register(user);
                result="success";
            }
        }
        return result;
    }

    @Override
    public void deleteuser(Long[] id) throws Exception {
        userMapper.deleteuser(id);
    }

    @Override
    public void updateuser(User user) throws Exception {
        Date time=new Date();
        user.setUpdatetime(time);
        userMapper.updateuser(user);
    }

    @Override
    public List<User> pages(String username,int page,int limit) throws Exception {
        String um=username;
        //启用分页类
        Page page1=new Page();
        //判断搜索条件是否为空
        if(um==null||um==""){
            page1.setStart(page,limit);
            page1.setSize(limit);
            page1.setUsername("");
        }else{
            page1.setStart(page,limit);
            page1.setSize(limit);
            page1.setUsername(username);
        }
        return userMapper.pages(page1);
    }
}
