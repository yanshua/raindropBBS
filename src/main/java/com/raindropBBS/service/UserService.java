package com.raindropBBS.service;

import com.raindropBBS.dao.po.User;
import com.raindropBBS.util.Page;

import java.util.List;

/**
 * @Title: raindropBBS
 * @Description: java类作用描述
 * @Author: 杨帅
 * @CreateDate: 2018/10/15 15:01
 * @Version: 1.0
 */
public interface UserService {
    public List<User> userlist()throws Exception;
    public String login(String account,String password)throws Exception;
    public String register(User user)throws Exception;
    //删除用户
    public void deleteuser(Long[] id)throws Exception;

    public void updateuser(User user)throws Exception;

    public List<User> pages(String username,int page,int limit)throws Exception;
}
