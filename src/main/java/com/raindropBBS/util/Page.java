package com.raindropBBS.util;

public class Page {
    private int start=0;
    private int size=0;
    private String username;

    public int getStart() {
        return start;
    }

    public void setStart(int page,int limit) {
        this.start = (page-1)*limit;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
