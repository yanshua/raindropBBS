/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50722
Source Host           : localhost:3306
Source Database       : raindropbbs

Target Server Type    : MYSQL
Target Server Version : 50722
File Encoding         : 65001

Date: 2018-10-15 10:50:06
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `accessory`
-- ----------------------------
DROP TABLE IF EXISTS `accessory`;
CREATE TABLE `accessory` (
  `acid` bigint(15) NOT NULL,
  `uid` bigint(15) NOT NULL,
  `cid` bigint(15) NOT NULL,
  `accessorycontent` text COMMENT '附件内容',
  `accessoryscore` int(15) DEFAULT NULL COMMENT '下载所需积分',
  `updatetime` datetime NOT NULL,
  `creatime` datetime NOT NULL,
  PRIMARY KEY (`acid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of accessory
-- ----------------------------

-- ----------------------------
-- Table structure for `administrator`
-- ----------------------------
DROP TABLE IF EXISTS `administrator`;
CREATE TABLE `administrator` (
  `aid` bigint(15) NOT NULL COMMENT '管理员id',
  `account` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `updatetime` datetime NOT NULL COMMENT '更新时间',
  `createtiem` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`aid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of administrator
-- ----------------------------

-- ----------------------------
-- Table structure for `card`
-- ----------------------------
DROP TABLE IF EXISTS `card`;
CREATE TABLE `card` (
  `cid` bigint(15) NOT NULL,
  `cardtitle` varchar(300) NOT NULL,
  `content` text NOT NULL COMMENT '帖子内容',
  `uid` bigint(15) NOT NULL,
  `access` int(5) NOT NULL COMMENT '帖子的浏览权限',
  `updatetime` datetime NOT NULL,
  `createtime` datetime NOT NULL,
  PRIMARY KEY (`cid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of card
-- ----------------------------

-- ----------------------------
-- Table structure for `discuss`
-- ----------------------------
DROP TABLE IF EXISTS `discuss`;
CREATE TABLE `discuss` (
  `did` bigint(15) NOT NULL,
  `cid` bigint(15) NOT NULL COMMENT '帖子id',
  `uid` bigint(15) NOT NULL COMMENT '用户id',
  `content` text NOT NULL,
  `parentid` bigint(15) NOT NULL COMMENT '父评论id',
  `updatetime` datetime NOT NULL,
  `createtime` datetime NOT NULL,
  PRIMARY KEY (`did`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of discuss
-- ----------------------------

-- ----------------------------
-- Table structure for `integral`
-- ----------------------------
DROP TABLE IF EXISTS `integral`;
CREATE TABLE `integral` (
  `iid` bigint(15) NOT NULL,
  `score` bigint(15) NOT NULL COMMENT '积分数量',
  `uid` bigint(15) NOT NULL COMMENT '用户id',
  `updatetime` datetime NOT NULL,
  `createtime` datetime NOT NULL,
  PRIMARY KEY (`iid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of integral
-- ----------------------------

-- ----------------------------
-- Table structure for `section`
-- ----------------------------
DROP TABLE IF EXISTS `section`;
CREATE TABLE `section` (
  `sid` bigint(15) NOT NULL COMMENT '版块id',
  `sectionname` varchar(100) NOT NULL,
  `updatetime` datetime NOT NULL,
  `createtime` datetime NOT NULL,
  PRIMARY KEY (`sid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of section
-- ----------------------------

-- ----------------------------
-- Table structure for `sectionmanager`
-- ----------------------------
DROP TABLE IF EXISTS `sectionmanager`;
CREATE TABLE `sectionmanager` (
  `smid` bigint(15) NOT NULL,
  `sid` bigint(15) NOT NULL COMMENT '版块id',
  `uid` bigint(15) NOT NULL COMMENT '用户id',
  `updatetime` datetime NOT NULL,
  `creatime` datetime NOT NULL,
  PRIMARY KEY (`smid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sectionmanager
-- ----------------------------

-- ----------------------------
-- Table structure for `systeminfo`
-- ----------------------------
DROP TABLE IF EXISTS `systeminfo`;
CREATE TABLE `systeminfo` (
  `systemname` varchar(150) NOT NULL,
  `versions` varchar(50) NOT NULL,
  `statement` text NOT NULL COMMENT '论坛声明',
  `updatetime` datetime NOT NULL,
  `createtime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of systeminfo
-- ----------------------------

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `uid` bigint(15) NOT NULL,
  `account` varchar(50) NOT NULL COMMENT '用户账号',
  `password` varchar(50) NOT NULL,
  `username` varchar(100) NOT NULL,
  `sex` varchar(10) NOT NULL,
  `startus` int(5) NOT NULL COMMENT '用户状态（0正常，1禁用）',
  `icon` varchar(300) DEFAULT NULL,
  `updatetime` datetime NOT NULL COMMENT '更新时间',
  `createtime` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'test', '098f6bcd4621d373cade4e832627b4f6', '张三', '男', '0', null, '2018-10-15 10:49:20', '2018-10-15 10:49:25');

-- ----------------------------
-- Table structure for `user_token`
-- ----------------------------
DROP TABLE IF EXISTS `user_token`;
CREATE TABLE `user_token` (
  `user_id` tinyint(4) NOT NULL,
  `token` varchar(100) NOT NULL,
  `expire_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_token
-- ----------------------------
